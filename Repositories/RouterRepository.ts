import {sendErrorToSentry} from '@/integrations/sentry'
import router from '@/orchestrated/router';
import {Module, SubModule} from "../EventTracking/TrackingModules";


interface IRouterRepository {
  getSubmodule(): SubModule
  getModule(): Module
}

export class RouterRepository implements IRouterRepository {
  private readonly _router: VueRouter:

  constructor() {
    this._router = router;
  }

  private _isSavedLanes () : boolean {
    return location?.search?.includes('savedLaneId')
  }

  public getSubmodule(): SubModule {
    // we need this condition for saved lanes because saved lanes is not a route yet
    if (this._isSavedLanes()) return SubModule.SAVED_LANES

    let subModule = ''

    try {
      subModule = (router.currentRoute.meta.subModule) as SubModule
    } catch (e) {
      sendErrorToSentry(new Error('Cannot find subModule in `meta` object for a router', e))
    }

    return subModule
  }

  public getModule(): Module {
    let module = ''

    try {
      module = (router.currentRoute.meta.module) as Module
    } catch (e) {
      sendErrorToSentry(new Error('Cannot find module in `meta` object for a router', e))
    }

    return module
  }
}
