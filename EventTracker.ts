import {RouterRepository} from "./Repositories/RouterRepository";
import {IEvent} from "./Events";

export type TrackedEvent = {
  name: string
  properties: {
    module: string
    submodule: string
  } & { [key: string]: any }
  timestamp?: Date
}

type TrackFunction = (key: string, properties: TrackedEvent) => void

export interface IEventTagger {
  tagEvent(key: string, properties: IEvent): void
}

// Example of AmplitudeTagger implementation with Router Repository
export class AmplitudeTagger implements IEventTagger {
  private readonly _trackEvent: TrackFunction;
  private readonly _routerRepository: RouterRepository;

  constructor(trackEvent: TrackFunction, routerRepository: RouterRepository) {
    this._trackEvent = trackEvent;
    this._routerRepository = routerRepository;
  }

  public tagEvent(key: string, properties: IEvent): void {
    const event = {
      name: key,
      properties: {
        ...properties,
        module: this._routerRepository.getModule(),
        submodule: this._routerRepository.getSubmodule()
      },
      timestamp: new Date()
    } as TrackedEvent;

    this._trackEvent(key, event);
  }
}
