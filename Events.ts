export interface IEvent {
  [key: string]: any;
}

export interface IEventTracker {
  SubscribeTo(events: EventBus): void;
}

export interface IEventBus {
  Emit<T extends IEvent>(emittedEvent: T): void;
  Subscribe<T extends IEvent>(action: (event: T) => void): void;
}

export class EventBus implements IEventBus {
  private readonly _consumers: { [eventType: string]: Array<(...args: any[]) => void> } = {};

  public Subscribe<T extends IEvent>(action: (event: T) => void): void {
    const eventType = (action as any).prototype.constructor.name;

    if (!(eventType in this._consumers)) {
      this._consumers[eventType] = [];
    }

    this._consumers[eventType].push(action);
  }

  public AddConsumer(consumer: IEventTracker): void {
    consumer.SubscribeTo(this);
  }

  public Emit<T extends IEvent>(emittedEvent: T): void {
    const eventType = typeof emittedEvent;

    if (!(eventType in this._consumers)) {
      return;
    }

    for (const action of this._consumers[eventType]) {
      action(emittedEvent);
    }
  }
}
