import {IEventTagger} from "../EventTracker";
import {IEventBus} from "../Events";
import {MarketplaceLoadMoreClickEvent} from "../EventTracking/Events/MarketplaceLoadMoreClickEvent";

interface IMarketplaceListRepository {
    getRenderedItems(): Array<{}>
    getItemsOfPage(number: number): Array<{}>
}

class MarketplaceLoadMoreUseCase {
    private _marketplaceListRepository: IMarketplaceListRepository;
    private _eventBus: IEventBus;

    constructor(marketplaceListRepository: IMarketplaceListRepository, eventBus: IEventBus) {
        this._marketplaceListRepository = marketplaceListRepository;
        this._eventBus = eventBus;
    }

    public execute(page: number): any {
        try {
            // example
            // const result = this._marketplaceHttpClient.fetchOrders(page);
            // this._marketplaceListRepository.persistOrders(page, result);
            //
            // const ordersToRender = this._marketplaceListRepository.getOrdersWithoutDuplicates(page)
            //
            // this._eventBus.Emit<MarketplaceLoadMoreClickEvent>(new MarketplaceLoadMoreClickEvent(
            //     "something",
            //     page,
            //     []
            //     )
            // )
            //
            // return ordersToRender;

        } catch (e) {
            // handle error path
            // send event if required
        }
    }
}