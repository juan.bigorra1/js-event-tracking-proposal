export enum Module {
    MARKETPLACE_LISTING_PAGE = 'marketplace-listing-page',
    MARKETPLACE_LIST_ITEM_DETAIL_PAGE = 'marketplace-odv',
    TENDERS_PAGE = 'tenders-page',
    SAVED_LANES = 'saved-lanes-page'
}

export enum SubModule {
    ODV_ACCEPT_NOW_WIDGET = 'marketplace-odv-accept-now-widget',
    ODV_BIDDING_WIDGET = "marketplace-odv-bidding-widget",
    SAVED_LANES = "saved-lanes",
}