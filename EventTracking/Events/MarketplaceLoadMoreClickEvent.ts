import {IEvent} from "../../Events";

export class MarketplaceLoadMoreClickEvent implements IEvent {
    property1: string;
    pageNumber: number;
    property3: Array<number>;

    constructor(property1: string, property2: number, property3: Array<number>) {
        this.property1 = property1;
        this.pageNumber = property2;
        this.property3 = property3;
    }
}