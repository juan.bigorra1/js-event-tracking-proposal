import {EventBus, IEvent, IEventTracker} from "../../Events";
import {IEventTagger} from "../../EventTracker";
import {TrackingEventKeys} from "../TrackingEventKeys";
import {MarketplaceLoadMoreClickEvent} from "../Events/MarketplaceLoadMoreClickEvent";

interface IFilterRepository {
    getAppliedFilters(): any // Add spec for filters
}

export class MarketplaceQueriesTracker implements IEventTracker {
    private _eventTagger: IEventTagger;
    private _filtersRepository: IFilterRepository;

    constructor(eventTagger: IEventTagger, filtersRepository: IFilterRepository) {
        this._eventTagger = eventTagger;
    }

    public SubscribeTo(events: EventBus): void {
        events.Subscribe<MarketplaceLoadMoreClickEvent>(this._onLoadMoreClickEvent);
    }

    private _onLoadMoreClickEvent(event: MarketplaceLoadMoreClickEvent) {
        const filtersApplied = this._filtersRepository.getAppliedFilters()

        const properties = {
            filters: {...filtersApplied},
            ...event
        } as IEvent;
        // handle event in here
        this._eventTagger.tagEvent(TrackingEventKeys.MARKETPLACE_LOAD_MORE_BUTTON, properties)
    }
}
